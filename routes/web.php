<?php

use \Illuminate\Http\Response;
use \Illuminate\Http\Request;
use App\Matrix;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function (Request $request) use ($router) {
    $q = $request->input('q');
    $d = $request->input('d');

    switch ($q) {
        case 'Degree':
            return new Response(env('DEGREES'), Response::HTTP_OK);
        case 'Email Address':
            return new Response(env('EMAIL_ADDRESS'), Response::HTTP_OK);
        case 'Name':
            return new Response(env('FULL_NAME'), Response::HTTP_OK);
        case 'Phone':
            return new Response(env('PHONE_NUMBER'), Response::HTTP_OK);
        case 'Ping':
            return new Response('OK', Response::HTTP_OK);
        case 'Position':
            return new Response(env('POSITION'), Response::HTTP_OK);
        case 'Puzzle':
            $equalityMatrix = Matrix::parse($d);
            $equalityMatrix->solve();
            return new Response($equalityMatrix->prettySolvedMatrix, Response::HTTP_OK);
        case 'Referrer':
            return new Response(env('REFERRER'), Response::HTTP_OK);
        case 'Resume':
            return new Response(env('RESUME_LINK'), Response::HTTP_OK);
        case 'Source':
            return new Response(env('SOURCE_LINK'), Response::HTTP_OK);
        case 'Status':
            return new Response(env('WORK_ELIGIBILITY'), Response::HTTP_OK);
        case 'Years':
            return new Response(env('YEARS'), Response::HTTP_OK);
        default:
            return new Response('Not found.', Response::HTTP_NOT_FOUND);
    };
});
