<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matrix extends Model
{
    const INDEX_TO_LETTER_MAP = [
        0 => 'A',
        1 => 'B',
        2 => 'C',
        3 => 'D',
    ];

    const LETTER_TO_INDEX_MAP = [
        'A' => 0,
        'B' => 1,
        'C' => 2,
        'D' => 3,
    ];


    protected $oneDimensionalMatrix;
    protected $solvedMatrix;

    public function __construct(array $oneDimensionalMatrix)
    {
        parent::__construct();
        $this->oneDimensionalMatrix = $oneDimensionalMatrix;
    }

    /**
     * Accepts the query string that bRealTime is passing for this challenge and returns
     * an instance of this class.
     *
     * @param string $matrixString
     * @return Matrix
     */
    public static function parse(string $matrixString): Matrix
    {
        $re = '/[\w+\s+:]/m';
        $matrix = str_split(preg_filter($re, '', $matrixString));
        return new Matrix($matrix);
    }

    /**
     * Pretty formats the solved matrix.
     *
     * @return string
     */
    public function getPrettySolvedMatrixAttribute(): string
    {
        // TODO: Make method not only applicable to a 4x4 ABCD matrix.
        $header = ' ABCD' . PHP_EOL;
        $row1 = 'A' . implode(array_slice($this->solvedMatrix, 0, 4)) . PHP_EOL;
        $row2 = 'B' . implode(array_slice($this->solvedMatrix, 4, 4)) . PHP_EOL;
        $row3 = 'C' . implode(array_slice($this->solvedMatrix, 8, 4)) . PHP_EOL;
        $row4 = 'D' . implode(array_slice($this->solvedMatrix, 12, 4));
        return $header . $row1 . $row2 . $row3 . $row4;
    }

    /**
     * Solve the input matrix and store the value.
     * 1. Generate the inequality pairs where keys are less than their respective values, e.g., [D => A, B => C, C => D].
     * 2. Create an order key to use when solving the matrix, e.g., [B, C, D, A];
     * 3. Insert the correct symbols into the array based on the order key generated in step 2.
     */
    public function solve()
    {
        $pairs = $this->createInequalityPairs();
        $order = $this->getOrderingKey($pairs);
        $this->solvedMatrix = $this->insertCorrectSymbols($order);
    }

    /**
     * Insert the >, <, and = symbols into the array.
     *
     * @param array $order
     * @return array
     */
    protected function insertCorrectSymbols(array $order): array
    {
        $unsolvedMatrix = array_slice($this->oneDimensionalMatrix, 0);
        foreach ($unsolvedMatrix as $index => $value) {
            if ($this->rowNumber($index) === $this->columnNumber($index)) {
                $unsolvedMatrix[$index] = '=';
                continue;
            }

            if ($value === '-') {
                $rowLetterRank = $order[$this->rowLetter($index)];
                $columnLetterRank = $order[$this->columnLetter($index)];

                $unsolvedMatrix[$index] = $rowLetterRank < $columnLetterRank ? '>' : '<';
                continue;
            }
        }
        return $unsolvedMatrix;
    }

    /**
     * Return the row/column values in the correct order based on the clues.
     *
     * @param array $pairs
     * @return array
     */
    protected function getOrderingKey(array $pairs): array
    {
        $holder = [];
        $skippedPairs = [];

        for ($i = 0; $i < count($pairs); $i++) {
            if (count($holder) === (count($pairs) + 1)) break;

            if ($i === 0) {
                $holder[] = key($pairs);
                $holder[] = current($pairs);
                next($pairs);
                continue;
            }

            if (in_array(key($pairs), $holder)) {
                $index = array_search(key($pairs), $holder);
                array_splice($holder, $index + 1, 0, current($pairs));
                next($pairs);
                continue;
            }

            if (in_array(current($pairs), $holder)) {
                $index = array_search(current($pairs), $holder);
                array_splice($holder, $index, 0, key($pairs));
                next($pairs);
                continue;
            }
            $skippedPairs[key($pairs)] = current($pairs);
            next($pairs);
        }

        foreach ($skippedPairs as $key => $value) {
            if (in_array($key, $holder)) {
                $index = array_search($key, $holder);
                array_splice($holder, $index + 1, 0, $value);
                continue;
            }

            if (in_array($value, $holder)) {
                $index = array_search($value, $holder);
                array_splice($holder, $index, 0, $key);
                next($pairs);
                continue;
            }
        }

        return array_flip($holder);
    }

    /**
     * Return an array of key value pairs where the keys are less than the values based on the puzzle input.
     *
     * @return array
     */
    protected function createInequalityPairs()
    {
        $pairs = [];

        for ($i = 0; $i < count($this->oneDimensionalMatrix); $i++) {
            $symbol = $this->oneDimensionalMatrix[$i];

            if ($symbol === '<' || $symbol === '>') {
                if ($symbol === '<') {
                    $pairs[$this->columnLetter($i)] = $this->rowLetter($i);
                } else {
                    $pairs[$this->rowLetter($i)] = $this->columnLetter($i);
                }
            }
        }

        return $pairs;
    }

    /**
     * Get the letter of the row based on the current index.
     *
     * @param int $index
     * @return string
     */
    protected function rowLetter(int $index): string
    {
        return Matrix::INDEX_TO_LETTER_MAP[$this->rowNumber($index)];
    }

    /**
     * Get the letter of the column based on the current index.
     *
     * @param int $index
     * @return string
     */
    protected function columnLetter(int $index): string
    {
        return Matrix::INDEX_TO_LETTER_MAP[$this->columnNumber($index)];
    }

    /**
     * Get the current row number based on the index.
     *
     * @param int $index
     * @return int
     */
    protected function rowNumber(int $index): int
    {
        return $index % 4;
    }


    /**
     * Get the current column number based on the index.
     *
     * @param int $index
     * @return int
     */
    protected function columnNumber(int $index): int
    {
        return $index / 4;
    }
}
